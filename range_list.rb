class RangeList
  class ParamsError < StandardError; end
  # 最小index
  MIN = 0
  # 最大index
  MAX = 1

  attr_accessor :range_list

  def initialize
    @range_list = []
  end

  def add(range)
    valid_params(range)

    return if range[MIN] >= range[MAX]

    # range_list is empty, new range, 不用比较，直接新建一个 range
    range_list << [range[MIN], range[MAX]] and return if range_list.length == 0

    left_min_index, left_max_index = bin_search(range[MIN])
    right_min_index, right_max_index = bin_search(range[MAX])

    # range on the left, range 在 range_list 的左边
    if left_min_index == -1
      add_left(
        range,
        right_min_index: right_min_index,
        right_max_index: right_max_index
      )
    # range on the right, range 在 range_list 的右边
    elsif right_max_index == -1
      add_right(
        range,
        right_min_index: right_min_index,
        left_min_index: left_min_index,
        left_max_index: left_max_index
      )
    # range in the middle, range 在 range_list 的中间
    else
      add_mid(
        range,
        left_min_index: left_min_index,
        left_max_index: left_max_index,
        right_min_index: right_min_index,
        right_max_index: right_max_index
      )
    end
  end

  def remove(range)

    valid_params(range)
    return if range_list.length == 0 || range[MIN] >= range[MAX]

    left_min_index, left_max_index = bin_search(range[MIN])
    right_min_index, right_max_index = bin_search(range[MAX])

    # range on the left, range 在 range_list 的左边
    if left_min_index == -1
      remove_left(
        range,
        right_min_index: right_min_index,
        right_max_index: right_max_index
      )
    # range on the right, range 在 range_list 的右边
    elsif right_max_index == -1
      remove_right(
        range,
        left_min_index: left_min_index,
        left_max_index: left_max_index,
        right_min_index: right_min_index
      )
    # range in the middle, range 在 range_list 的中间
    else
      remove_mid(
        range,
        left_min_index: left_min_index,
        left_max_index: left_max_index,
        right_min_index: right_min_index,
        right_max_index: right_max_index
      )
    end
  end

  def print
    str = range_list.map { |range| "[#{ range[MIN] }, #{ range[MAX] })" }.join(" ")
    puts str
    str
  end

  private

  #
  # Binary search, 二分查找
  #
  # @param number [Integer]
  #
  # @return [Array] example: [0, 2], last index and next index,
  #         上一个 range 范围的 index， 下一个 range 范围的index, 如果没有 range 了，就返回 -1
  #
  def bin_search(number)
    return [-1, -1] if range_list.length == 0

    max_index = range_list.length - 1
    min_index = 0
    left, right = min_index, max_index

    while left <= right
      mid = left + (right - left) / 2

      if range_list[mid][MIN] > number
        if mid - 1 >= min_index
          return [mid - 1, mid] if range_list[mid - 1][1] <= number
          right = mid - 1
        else
          return [-1, mid]
        end
      elsif range_list[mid][MAX] <= number
        if mid + 1 <= max_index
          return [mid, mid + 1] if range_list[mid + 1][MIN] > number
          left = mid + 1
        else
          return [mid, -1]
        end
      elsif range_list[mid][MIN] <= number && range_list[mid][MAX] > number
        return [mid, mid]
      end
    end

    return [-1, -1]
  end

  #
  # clear extra range_list item, 清除额外的 range_list 项
  #
  # @param left_index [Integer]  begin index, 开始的下标
  # @param right_index [Integer]  min end index, 结束的下标
  #
  def delete_extra_range(left_index, right_index)

    (left_index..right_index).to_a.reverse.each do |i|
      range_list.delete_at(i)
    end
  end

  def valid_params(range)
    if range.nil? || range.length != 2
      raise ParamsError, "参数必须是一个数组有2个值"
    end
    if !range[MIN].is_a?(Integer) || !range[MAX].is_a?(Integer)
      raise ParamsError, "参数需要是int数组"
    end
  end

  #
  # @param range [Array]
  # @param position [Hash] right_min_index and right_max_index
  #
  def add_left(range, position)
    right_min_index, right_max_index = position[:right_min_index], position[:right_max_index]
    # create
    if range[MAX] < range_list[0][MIN]
      range_list.insert(0, [range[MIN], range[MAX]])
    # update
    else
      # range_list[0] min value equal range min value, range_list 的最小值为 range 最小值
      range_list[0][MIN] = range[MIN]

      max_index = [right_min_index, right_max_index].min
      # the range max value, [上一个区间的最大值, range 的最大值].max
      max_value = [range_list[max_index][MAX], range[MAX]].max

      # range_list[0] max value equal range max, 把 range 的最大值赋值给 range_list[0] 的最大值
      range_list[0][MAX] = max_value

      # clear extra range_list item
      delete_extra_range(1, max_index)
    end
  end

  #
  # @param range [Array]
  # @param position [Hash] right_min_index, left_min_index and left_max_index
  #
  def add_right(range, position)
    right_min_index, left_min_index, left_max_index = \
    position[:right_min_index], position[:left_min_index], position[:left_max_index]

    # create, insert new range, 新建一个 range
    if range[MIN] > range_list[right_min_index][MAX]
      range_list.insert(right_min_index + 1, [range[MIN], range[MAX]])
    # update
    else
      # update range_list last item, 更新 range_list 最后一项
      range_list[right_min_index][MAX] = range[MAX]
      min_index = [left_min_index, left_max_index].max

      min_value = [range_list[min_index][MIN], range[MIN]].min
      range_list[right_min_index][MIN] = min_value

      # clear extra range_list item
      delete_extra_range(min_index, right_min_index - 1)
    end
  end

  #
  # @param range [Array]
  # @param position [Hash] left_min_index, left_max_index, right_min_index and right_max_index
  #
  def add_mid(range, position)
    left_min_index, left_max_index, right_min_index, right_max_index = \
    position[:left_min_index], position[:left_max_index], position[:right_min_index], position[:right_max_index]

    min_index = [left_min_index, left_max_index].max
    max_index = [right_min_index, right_max_index].min

    min_value = [range_list[min_index][MIN], range[MIN]].min
    max_value = [range_list[max_index][MAX], range[MAX]].max

    # clear extra range_list item
    delete_extra_range(min_index, max_index)

    # insert new range, 插入新的 range
    if min_value < max_value
      range_list.insert(min_index, [min_value, max_value])
    end
  end

  #
  # @param range [Array]
  # @param position [Hash] right_min_index and right_max_index
  #
  def remove_left(range, position)
    right_min_index, right_max_index = position[:right_min_index], position[:right_max_index]
    # remove
    if range[MAX] > range_list[0][MIN]
      max_index = [right_min_index, right_max_index].min
      max_value = [range_list[max_index][MAX], range[MAX]].max

      # clear extra range_list item
      delete_extra_range(0, max_index)

      # insert new range, 插入新的 range
      if max_value > range[MAX]
        range_list.insert(0, [range[MAX], max_value])
      end
    end
  end

  #
  # @param range [Array]
  # @param position [Hash] right_min_index, left_min_index and left_max_index
  #
  def remove_right(range, position)
    right_min_index, left_min_index, left_max_index = \
    position[:right_min_index], position[:left_min_index], position[:left_max_index]
    # remove
    if range[MIN] < range_list[left_max_index][MAX]
      min_index = [left_min_index, left_max_index].max
      min_value = [range_list[min_index][MIN], range[MIN]].min

      # clear extra range_list item
      delete_extra_range(min_index, right_min_index)

      # insert new range, 插入新的 range
      if min_value < range[MIN]
        range_list.insert(min_index, [min_value, range[MIN]])
      end
    end
  end

  #
  # @param range [Array]
  # @param position [Hash] left_min_index, left_min_index, right_min_index and right_max_index
  #
  def remove_mid(range, position)
    left_min_index, left_max_index, right_min_index, right_max_index = \
    position[:left_min_index], position[:left_max_index], position[:right_min_index], position[:right_max_index]

    min_index = [left_min_index, left_max_index].max
    max_index = [right_min_index, right_max_index].min

    min_value = [range_list[min_index][MIN], range[MIN]].min
    max_value = [range_list[max_index][MAX], range[MAX]].max

    # clear extra range_list item
    delete_extra_range(min_index, max_index)

    # insert smaller range, 插入较小的 range
    if min_value < range[MIN]
      range_list.insert(min_index, [min_value, range[MIN]])
      min_index += 1
    end

    # insert larger range, 插入较大的 range
    if max_value > range[MAX]
      range_list.insert(min_index, [range[MAX], max_value])
    end
  end
end