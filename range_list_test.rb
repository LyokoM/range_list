require './range_list'

class RangeListTest

  def self.test_data
    rl = RangeList.new
    rl.add([1, 5])
    assert_result(rl.print, '[1, 5)')
    rl.add([10, 20])
    assert_result(rl.print, '[1, 5) [10, 20)')
    rl.add([10, 20])
    assert_result(rl.print, '[1, 5) [10, 20)')
    rl.add([20, 20])
    assert_result(rl.print, '[1, 5) [10, 20)')
    rl.add([20, 21])
    assert_result(rl.print, '[1, 5) [10, 21)')
    rl.add([2, 4])
    assert_result(rl.print, '[1, 5) [10, 21)')
    rl.add([3, 8])
    assert_result(rl.print, '[1, 8) [10, 21)')
    rl.remove([10, 10])
    assert_result(rl.print, '[1, 8) [10, 21)')
    rl.remove([10, 11])
    assert_result(rl.print, '[1, 8) [11, 21)')
    rl.remove([15, 17])
    assert_result(rl.print, '[1, 8) [11, 15) [17, 21)')
    rl.remove([3, 19])
    assert_result(rl.print, '[1, 3) [19, 21)')
  end

  private

  def self.assert_result(args1, args2)
    if args1 == args2
      puts "**********"
    else
      puts "error"
    end
  end
end

RangeListTest.test_data