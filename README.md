```rb
RangeList
In this part of the interview process, we'd like you to come up with an algorithm to solve the problem as described below. The problem itself is quite simple to solve. The solution needs to be in Ruby or Golang. What we are mainly looking for in this test (other than that the solution should work) is, how well you actually write the code.
When you are done, please submit your code to gitlab.com as a public git repo and email us the link. We want to see how you write production-quality code in a team setting. Specifically, we are looking
for: simple, clean, readable and maintainable code, for example:
Space and time complexity. Space and Time complexity can define the effectiveness of your
algorithm.
Code organization and submission format. Things like code organization, readability, documentation, testing and deliverability are most important here.
Your mastery of idiomatic Ruby/Go programming. We understand that you may not have much experience with Ruby/Go. We encourage you to take some time to research modern Ruby and best practices, and try your best to apply them when writing your test solution.
You can choose either Ruby template or Go template.

# Task: Implement a class named 'RangeList'
# A pair of integers define a range, for example: [1, 5). This range includes integers: 1, 2, 3, and
4.
# A range list is an aggregate of these ranges: [1, 5), [10, 11), [100, 201)
# NOTE: Feel free to add any extra member variables/functions you like.
class RangeList
def add(range)
# TODO: implement add
end
def remove(range)
# TODO: implement remove
end
def print
# TODO: implement print
end end
rl = RangeList.new
rl.add([1, 5])
rl.print
// Should display: [1, 5)
rl.add([10, 20])
rl.print
RangeList
2
// Should display: [1, 5) [10, 20)
rl.add([20, 20])
rl.print
// Should display: [1, 5) [10, 20)
rl.add([20, 21])
rl.print
// Should display: [1, 5) [10, 21)
rl.add([2, 4])
rl.print
// Should display: [1, 5) [10, 21)
rl.add([3, 8])
rl.print
// Should display: [1, 8) [10, 21)
rl.remove([10, 10])
rl.print
// Should display: [1, 8) [10, 21)
rl.remove([10, 11])
rl.print
// Should display: [1, 8) [11, 21)
rl.remove([15, 17])
rl.print
// Should display: [1, 8) [11, 15) [17, 21)
rl.remove([3, 19])
rl.print
// Should display: [1, 3) [19, 21)
// Task: Implement a struct named 'RangeList'
// A pair of integers define a range, for example: [1, 5). This range includes integers: 1, 2, 3, and 4.
// A range list is an aggregate of these ranges: [1, 5), [10, 11), [100, 201)
// NOTE: Feel free to add any extra member variables/functions you like.
package main
type RangeList struct {
//TODO: implement
}
func (rangeList *RangeList) Add(rangeElement [2]int) error {
//TODO: implement
return nil
}
func (rangeList *RangeList) Remove(rangeElement [2]int) error {
//TODO: implement
return nil
}
func (rangeList *RangeList) Print() error {
//TODO: implement
return nil
}

RangeList 3
func main() {
rl := RangeList{}
rl.Add([2]int{1, 5})
rl.Print()
// Should display: [1, 5)
rl.Add([2]int{10, 20})
rl.Print()
// Should display: [1, 5) [10, 20)
rl.Add([2]int{20, 20})
rl.Print()
// Should display: [1, 5) [10, 20)
rl.Add([2]int{20, 21})
rl.Print()
// Should display: [1, 5) [10, 21)
rl.Add([2]int{2, 4})
rl.Print()
// Should display: [1, 5) [10, 21)
rl.Add([2]int{3, 8})
rl.Print()
// Should display: [1, 8) [10, 21)
rl.Remove([2]int{10, 10})
rl.Print()
// Should display: [1, 8) [10, 21)
rl.Remove([2]int{10, 11})
rl.Print()
// Should display: [1, 8) [11, 21)
rl.Remove([2]int{15, 17})
rl.Print()
// Should display: [1, 8) [11, 15) [17, 21)
rl.Remove([2]int{3, 19})
rl.Print()
// Should display: [1, 3) [19, 21)
}
```
## Usage

Execute in `irb`

```rb
require './range_list'

rl = RangeList.new
rl.add([1, 5])
rl.print
# Should display: [1, 5)
rl.add([10, 20])
rl.print
# Should display: [1, 5) [10, 20)
rl.add([20, 20])
rl.print
# Should display: [1, 5) [10, 20)
rl.add([20, 21])
rl.print
# Should display: [1, 5) [10, 21)
rl.add([2, 4])
rl.print 
# Should display: [1, 5) [10, 21)
rl.add([3, 8])
rl.print
# Should display: [1, 8) [10, 21)
rl.remove([10, 10])
rl.print 
# Should display: [1, 8) [10, 21)
rl.remove([10, 11])
rl.print 
# Should display: [1, 8) [11, 21)
rl.remove([15, 17])
rl.print 
# Should display: [1, 8) [11, 15) [17, 21)
rl.remove([3, 19])
rl.print 
# Should display: [1, 3) [19, 21)
```
